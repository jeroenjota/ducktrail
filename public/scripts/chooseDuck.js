var ducklist = new Vue({
  el: '#ducklist',
  data: {
    ducks: [],
    ducksChosen: [],
    languages: [
      'Nederlands',
      'Engels',
      'Duits',
      'Frans'
    ]
  },
  // Wordt gedraaid wanneer dit component ingeladen wordt
  mounted: function() {
    this.getDucks()
  },
  // De waardes gedefiniëerd zijn 'computed' en kunnen gebruikt worden als 'data' waardes
  computed: {
    sortedDucks: function() {
      return this.ducks.sort( function(a, b) {
        const een = a.name.toUpperCase()
        const twee = b.name.toUpperCase()
        if (een > twee){
          return 1
        } else {
          return -1
        }
      })
    }

  },
  methods: {
    toggleChosen: function(eend){
      eend.chosen = !eend.chosen
      if (eend.chosen){
        this.ducksChosen.push(eend)
        for (var i = 0; i < this.ducks.length; i++){
          if (this.ducks[i].name === eend.name) {
            this.ducks.splice(i,1)
          }
        }
      } else {
        this.ducks.push(eend)
        this.ducks.sort(
          function(a, b) {
            const een = a.name.toUpperCase()
            const twee = b.name.toUpperCase()
            if (een > twee){
              return 1
            } else {
              return -1
            }
          }
        )

        for (var i = 0; i < this.ducksChosen.length; i++){
          if (this.ducksChosen[i].name === eend.name) {
            this.ducksChosen.splice(i,1)
          }
        }

      }
    },
    submit: function(e) {
      var form = document.getElementById('tourform')
      var formData = new FormData(form)
      formData.append('eenden', this.ducksChosen)

      axios.post('/antwform', formData)
      .then(response => {
        console.log(response.data)
      })

    },
    getDucks: function() {
      axios
        .get('/api/v1/ducks')
        .then(response => {
          console.log('ducks:', response.data)
          this.ducks = response.data.map((duck) => {
            duck.chosen = false
            return duck
          })
        })
        .catch(error => {
          console.error('kon ducks niet ophalen', error)
        })
    }
  }
})
