// main app fo Ductrail App

var http       = require("http")
var express    = require('express')
var fs         = require('fs')
var path       = require('path')
var moment     = require('moment')
var bodyparser = require('body-parser')
var pdfMake    = require('pdfmake/build/pdfmake.js');
var pdfFonts   = require('pdfmake/build/vfs_fonts.js');

pdfMake.vfs = pdfFonts.pdfMake.vfs;

var app = express()

moment.locale("nl")

app.use(bodyparser.urlencoded({extended: true}))
app.use(bodyparser.json())
app.use(express.static("public"))

app.get('/', function(req, res){
  res.render('index.pug')
})

// Iemand wil de eenden weten. Lees bestand in, vorm om naar array en stuur door.
app.get('/api/v1/ducks', function(req, res) {
	var pad = path.join(__dirname, 'public', 'data', 'eendenlijst.txt')
	fs.readFile(pad, function(error, duckBuffer) {
		if (error) {
			console.error('Kon bestand niet lezen', error)
			return res.status(500).json({error: error.message})
		}
		var ducks = duckBuffer.toString()
		// Split op newline, sorteer abc, maak nieuw array met de map function. De functie wordt aangeroepen voor elk item
		// in een array.
		var json = ducks.split('\r\n').sort().map(function(duck) {
			// Deze syntax pakt item 0 en 1 uit een array en stopt ze in de name en isofix vars
			// isofix is undefined als hij niet bestaat
			var [ name, isofix ] = duck.split(';') // duck = "eend naam;[isofix]"
			isofix = Boolean(isofix)
			// Deze syntax creëert een object met: { name: "Eend naam", isofix: false/true }
			return { name, isofix }
		})
		// Respond met array
		res.json(json)
	})
})

app.get('/tour', function(req, res){
  var ducks = function readDucks() {
    var pad = path.join(__dirname, '/public/data')
    var ducks = fs.readFileSync( pad + '/eendenlijst.txt').toString().split('\r\n')
    duckList = ducks.sort()
    var duckArr = []
    for(var i = 0; i<duckList.length; i++){
      var obj = { 'name' : duckList[i] }
      duckArr.push(obj)
    }
    return duckArr
  }
  res.render('tour.pug', {ducks: ducks(), moment:moment})

})

app.get('/about', function(req, res){
  res.render('about.pug')
})

app.post('/antwform', function (req, res){
// generate antw formulieren
  // var docDefinition = antwForm(req.body)
  console.log(req.body)
  // if(req.body.AnswerForms)

  res.render('antwform.pug')
})


app.listen(8000)
console.log('Het gebeurd allemaal op port 8000')
